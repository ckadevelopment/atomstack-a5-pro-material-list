# Atomstack A5 Pro+ 40w Settings
***Detailed work must be scanned vertically.***   
***"Banding" can be eliminated by tighting x-axis carriage wheels and shimming between the laser and sliding-mount.***  

*Speed is in mm/m and power is in percentage (100% is s-max of 1000).
### Wood Engraving
- General
  - 2500/40% (2500-4000)
  - 0.1 line-interval
  - 254 dpi
- Bamboo
  - 3500/40%
  - 0.1 line-interval
  - 254 dpi
### Wood Cutting
- General (10mm)
  - 500/100%
  - 15 Passes
### Metal Engraving
- 350/100%
- 0.1 line-interval
- 254 dpi
### Tile Engraving
- 1000/80%
- 0.1 line-interval
- 254 dpi
### Paper
  - 3000/25%
  - 0.1 lin-interval
  - 254 dpi
### Slate
  - 1000/80%
  - 0.1 lin-interval
  - 254 dpi
